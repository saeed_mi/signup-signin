function register(){
    var form = document.getElementById('register_form');
    var formData = new FormData(form);
    $.ajax({
        url: "/api/register",
        type: "POST",
        data:  formData,
        contentType: false,
        cache: false,
        processData:false,
        success:function(result) {
            $( '.alert-message' ).html('<div class="alert alert-success text-center">You have successfully registered</div>');
        },
        error:function(result) {
            var errors = result.responseJSON;
            let errorsHtml = '<div class="alert alert-danger"><ul>';
            $.each( errors.error , function( key, value ) {
                errorsHtml += '<li>' + value[0] + '</li>';
            });
            errorsHtml += '</ul></di>';
            $( '.alert-message' ).html( errorsHtml );
        }
    });
}
function login(){
    var form = document.getElementById('login_form');
    var formData = new FormData(form);
    $.ajax({
        url: "/api/login",
        type: "POST",
        data:  formData,
        contentType: false,
        cache: false,
        processData:false,
        success:function(result) {
            localStorage.setItem('access_token', result.token);
            location.replace("/dashboard?token="+result.token);
        },
        error:function(result) {
            var errors = result.responseJSON;
            if(result.status==400) var errorsHtml = '<div class="alert alert-danger text-center">'+errors.error+'</div>';
            else{
                var errorsHtml = '<div class="alert alert-danger"><ul>';
                $.each( errors.error , function( key, value ) {
                    errorsHtml += '<li>' + value[0] + '</li>';
                });
                errorsHtml += '</ul></di>';
            }
            $( '.alert-message' ).html( errorsHtml );
        }
    });
}

function logout(){
    var formData = new FormData();
    formData.append('token',localStorage.getItem('access_token'))
    $.ajax({
        url: "/api/logout",
        type: "POST",
        data:  formData,
        contentType: false,
        cache: false,
        processData:false,
        success:function(result) {
            localStorage.removeItem('access_token');
            location.replace("/");
        },
        error:function(result) {

        }
    });
}
