@extends("global.master")
@section("title","Signin")
@section("root")
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-sm-12">
                    <div class="text-center py-4">
                        <h1>Creative Login Form</h1>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <form id="login_form">
                                <div class="row">
                                    <div class="col-12 alert-message">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="username" placeholder="Username" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="password" class="form-control" name="password" placeholder="Password" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <span class="btn btn-custom w-100" onclick="login()">Login</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <p>Don't have an Account? <a href="/register" class="">Signup Now!</a></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
