@extends("global.master")
@section("title","Signup")
@section("root")
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-sm-12">
                    <div class="text-center py-4">
                        <h1>Creative SignUp Form</h1>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <form id="register_form">
                                <div class="row">
                                    <div class="col-12 alert-message">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="username" placeholder="Username" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="email" placeholder="Email" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="password" class="form-control" name="password" placeholder="Password" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" name="confirm_check" id="confirm_check">
                                            <label class="form-check-label" for="confirm_check">
                                                I Agree To The Terms & Conditions
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <span class="btn btn-custom w-100" onclick="register()">SIGNUP</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <p>Do you have an Account? <a href="/" class="">Login Now!</a></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
