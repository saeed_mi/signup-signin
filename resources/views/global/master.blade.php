<!doctype html>
<html lang="fa">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/jquery-3.6.0.min.js"></script>
    <script src="/assets/main.js"></script>
    <link rel="stylesheet" href="/assets/style.css">
    <title>@yield("title")</title>
</head>
<body>
@yield("root")
</body>
</html>
